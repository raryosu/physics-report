N=100;
SCALE=50;

PI=3.14159265;
for (t=0;t<N;t++) {
line = activeDocument.pathItems.add();

y0 = Math.sin(t/N*2*PI)*SCALE;
y1 = Math.sin((t+1)/N*2*PI)*SCALE;

line.setEntirePath([[t,y0],[t+1,y1]]);
line.filled = false;
line.stroked = true;
line.selected = true;
}

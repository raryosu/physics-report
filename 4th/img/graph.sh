#!/bin/sh
#
#title, 横軸の最小値、最大値を引数に入れると、
#gnuplotによるsin(x)のプロットを行ない、ポストスクリプトで保存する

#--- 引数チェック、格納。使用方法表示。
### $#はコマンドラインに続く文字列の数。

### 引数は、図のタイトル、xの最小値、xの最大値の３つ。
THETA=(0 0.785 1.57 2.355 3.14)

for rength in ${THETA[@]}; do
gnuplot <<EOF
set terminal pdf enhanced size 4in, 4in
set size square
set parametric
set samples 4096
set output "11_$rength.pdf"
plot sin(2*3.14*t*1), sin(2*3.14*t*1+$rength)
set output "12_$rength.pdf"
plot sin(2*3.14*t*1), sin(2*3.14*t*2+$rength)
set output "13_$rength.pdf"
plot sin(2*3.14*t*1), sin(2*3.14*t*3+$rength)
set output "21_$rength.pdf"
plot sin(2*3.14*t*2), sin(2*3.14*t*1+$rength)
set output "23_$rength.pdf"
plot sin(2*3.14*t*2), sin(2*3.14*t*3+$rength)
set output "31_$rength.pdf"
plot sin(2*3.14*t*3), sin(2*3.14*t*1+$rength)
set output "32_$rength.pdf"
plot sin(2*3.14*t*3), sin(2*3.14*t*2+$rength)
EOF
done


\documentclass[10.5pt,a4j,fleqn]{ltjsarticle}
%タイトルページ
% \usepackage{./layouts/titlePage}
%ソースコード表示
\usepackage{listings}
%複数列
\usepackage{multicol}
%表
\usepackage{enumitem}
%画像
\usepackage{graphicx}
\usepackage{asymptote}
\usepackage{float}
% 図
\usepackage{tikz}
\usepackage{circuitikz}
\usepackage{chemfig}
\usepackage{wrapfig}
%フレーム
\usepackage{framed}
\definecolor{shadecolor}{gray}{0.80}
%枠
\usepackage{ascmac}
%フォント
\usepackage{fontspec}
\usepackage{luatexja-fontspec}
% 化学
\usepackage[version=3]{mhchem}
%数式
\usepackage{amsmath,amssymb}
\usepackage{mathtools}
\makeatletter
\makeatother
\newcommand{\diff}[0]{\mathrm{d}}
%biber
\usepackage{csquotes}
\usepackage[backend=biber,style=numeric]{biblatex}
%\bibliographystyle{jplain}
\addbibresource{ref.bib}
\DeclareFieldFormat*{volume}{\mkbibbold{#1}} % 巻数を太字にする
\DeclareFieldFormat*{title}{\mkbibquote{#1\adddot}}  % タイトルにダブルクオーテーションをつける
\DeclareFieldFormat[article]{book}{\mkbibemph{#1}}  % article要素のbookを斜体にする
\renewcommand{\labelnamepunct}{\addcolon\addspace} % eg., J. Smith: "A great paper."
\renewbibmacro{in:}{} % in: Some journal の "in:" を取る
%Si単位系
\usepackage{siunitx}
%定義, 証明
\usepackage[thmmarks]{ntheorem}
%数式環境内での自動改ページ
\allowdisplaybreaks
\newtheorem{theorem}{定理}[section]
\newtheorem{prop}{命題}
\newtheorem{lemma}[theorem]{補題}
\newtheorem{definision}[theorem]{定義}
\newtheorem{note}{注}
\theorembodyfont{\upshape}
\theoremsymbol{\ensuremath{\heartsuit}}
\theoremseparator{}
\theoremsymbol{\rule{1ex}{1ex}}
\newtheorem{proof}{証明}
\newtheorem{property}{性質}
\newtheorem{example}{例}
\renewcommand{\thenote}{}
\renewcommand{\theproof}{}
\theoremstyle{plain}
\theoremseparator{.}
\theoremprework{\bigskip\hrule}
\theorempostwork{\hrule\bigskip}
\newtheorem{question}{問}


\author{Ryosuke Hagihara}
\title{応用物理実験 11 放射線計測}
\date{\today}

\begin{document}

\makeatletter
\newenvironment{tablehere}
  {\def\@captype{table}}
  {}

\newenvironment{figurehere}
  {\def\@captype{figure}}
  {}
\makeatother
\section{目的}
霧箱は「肉眼で見ることができない放射線を視認可能にする装置」である。
本実験は「高温拡散型霧箱」を使用して$\alpha$線源(Pb-210)から放出される
$\alpha$線を観測し，$\alpha$線の飛跡の長さ(飛程)を調べる。
そして$\alpha$粒子の運動エネルギーと比較して，飛程とエネルギーの関連を考察する。

\section{理論}
\subsection*{(a)霧箱の動作原理}
霧箱の原理は過冷却などを用いて霧を発生させた気体の中に荷電粒子や放射線を入射させると
気体分子のイオン化（電離）が起こることで，そのイオンが凝結核として飛跡が観測される（図\ref{fig:tool}）。
この原理は「飛行機雲ができる仕組み」と同じで，「電離作用でできたイオン」は
「飛行機から放出された塵など」に相当し，これらが凝結することで視認が可能となる。
\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.7\linewidth]{img/tool.pdf}
        \caption{飛跡の仕組み}
        \label{fig:tool}
  \end{center}
\end{figure}

\subsection*{(b)飛程}
飛程とは，$\alpha$粒子などの重荷電粒子が物質に入社して，原子と散乱や電離，
励起を繰り返しながら全エネルギーを失うまでに進んだ距離のことをいう。
本来なら放射性物質から放出された$\alpha$粒子群のエネルギーは均一なので，
物質内での飛程も全て等しくなるはずだが，実際には大小様々な飛程のものが存在している。
これは，$\alpha$粒子による物質原子の電離・励起作用の回数が異なることによって
生じた統計的変動であり，電離・励起作用が確率現象であることを示している。

$\alpha$粒子のエネルギー$E$[\si{\mega\electronvolt}]と空気中での飛程$R$[\si{\centi\metre}]との
関係式は
\begin{align}
  \label{eq:ev-cm}
  R=0.318E^{3/2}
\end{align}
で求められる。図\ref{fig:ev-cm}に式\ref{eq:ev-cm}のグラフを示す。

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.7\linewidth]{img/graph.pdf}
        \caption{飛跡とエネルギーの関係}
        \label{fig:ev-cm}
  \end{center}
\end{figure}
なお，実験は空気中ではなく，気化したエチレングリコールが過飽和状態となっている
霧箱の観察槽で行われるが，観察槽内の気化したエチレングリコールは空気と混合しており，
密度が空気と変わらないため飛程の式は空気中と同じものを使う。

\subsection*{(c)エチレングリコール}
主に溶媒，不凍液，合成原料として広く用いられている二価アルコールの一種である。
粘り気のある無職な液体で，水などの極性溶媒に溶けやすい。
分子式は\ce{C2H6O2}，分子量は62.07，融点は\SI{-12.7}{\degreeCelsius}(\SI{470}{\kelvin})，
気化点は\SI{20}{\degreeCelsius}，引火点は\SI{111}{\degreeCelsius}，発火点は\SI{398}{\degreeCelsius}である。
また，\SI{20}{\degreeCelsius}での蒸気と空気混合気体との相対密度は空気を1とすれば，
エチレングリコールの密度は1.00となっているので，気化したエチレングリコールは
空気中と変わらないものとする。

\subsection*{(d)線源(鉛)}
本実験では$\alpha$線源に鉛の放射性同位元素であるPb-210を使用する。
この線源の運動エネルギーは$E=5.407$\si{\mega\electronvolt}である。

\section{使用器具}
高温拡散型霧箱(WH-20)，$\alpha$線源(鉛の放射性同位体Pb-210)，観測用アクリル板

\section{報告}
\begin{itemize}
  \item[(a)]$\alpha$線源の飛跡の様子を観察してわかったこと\\
    飛跡は$\alpha$線が横に伸びていくのみではなく，下に落ちていくように伸びていた。
    上から見ると平面的な伸び方をしているようだったが，立体的な飛跡を描いていた。
    これらの線はアルコールが凝固したものであり，質量があるためこのように立体的に落下していく。

    また，長さや線の濃さも様々で大小様々な飛跡が，様々なタイミングで出現していた。

  \item[(b)]ヒストグラムの作成\\
    測定値の表およびヒストグラムを表\ref{table:table}および図\ref{fig:gram}に示す。\\
    \begin{table}[h]
      \caption{飛程の距離と回数}
      \centering\begin{tabular}{|c|cccccccc|}\hline
        飛程[cm] & 0.5 & 1.0 & 1.5 & 2.0 & 2.5 & 3.0 & 3.5 & 4.0\\\hline
        回数 & 22 & 73 & 76 & 65 & 42 & 16 & 3 & 3\\\hline
      \end{tabular}
      \label{table:table}
    \end{table}
    \begin{figure}[h]
      \begin{center}
        \includegraphics[width=0.7\linewidth]{img/gram.pdf}
        \caption{飛跡とエネルギーの関係のヒストグラム}
        \label{fig:gram}
      \end{center}
    \end{figure}
    飛程の最大値，最大値から$1/3$までの飛程平均値，全体の飛程平均値を表\ref{table:data}に示す。
    なお，最大値から$1/3$までの飛程平均値$\bar{n}$は以下のように求めた。
    \begin{align*}
      \bar{n} &= \left\{(4.0 \times 3) + (3.5 \times 3) + (3.0 \times 16) + (2.5 \times 25) + (2.0 \times 36)\right\} / 100\\
      &= 205 / 100\\
      &= 2.05
    \end{align*}
    \begin{table}[h]
      \caption{ヒストグラムから読み取れるデータ}
      \centering\begin{tabular}{c|c}\hline
        & データ[\si{\centi\metre}]\\\hline
        最大値 & 4\\
        最小値 & 0.5\\
        最大値から$1/3$までの飛程平均値 & 2.05\\
        全体の飛程平均値 & 1.68\\\hline
      \end{tabular}
      \label{table:data}
    \end{table}

  \item[(c)]正しい飛程の求め方の考察

    $\alpha$線源のエネルギーは式\ref{eq:ev-cm}より以下のようになる。
    \begin{table}[h]
      \caption{飛程と$\alpha$線源のエネルギーの関係}
      \centering\begin{tabular}{|c|cccc|}\hline
        飛程[cm] & 4.0 & 0.5 & 2.05 & 1.68 \\\hline
        エネルギー[MeV] & 5.41 & 1.35 & 3.46 & 3.03 \\\hline
      \end{tabular}
      \label{table:table}
    \end{table}
    本実験で用いた線源Pb-210の運動エネルギーは\SI{5.407}{\mega\electronvolt}であることから，
    正しい飛程を求めるためには，飛程4.0\si{\centi\metre}(最大値)の際のエネルギーを用いるのが
    適切である。

  \item[(d)]なぜ飛程が存在するかの考察

    本来であれば正しい慣性の法則によりいつまでも運動を続けるはずだが
    $\alpha$粒子により物質原子に対し電離・励起作用を繰り返すため，
    運動エネルギーをいずれ失い，それが飛程となって現れる。

    さらに$\alpha$粒子による物質原子の電離・励起作用は確率現象であるため，
    観測できる飛程はまちまちである。
    今回の実験では，\SI{1.5}{\centi\metre}地点で全エネルギーを失った放射線がもっとも多く，
    正しい飛程である\SI{4.0}{\centi\metre}まで到達した放射線は非常に少なかった。
\end{itemize}

\section{考察}
$\alpha$線源の飛程として採用すべきである\SI{4.0}{\centi\metre}について，
なぜもっとも観測回数が少なくなっているかが疑問であったが，アルコールとの衝突時にエネルギーが
減衰していくためそれ以前で消滅してしまうことが多いのだと考えていた。
しかし，解説には電離・励起作用によるものと記載されており，若干認識に誤りがあることがわかった。
電離・励起作用についてもう少し詳細に調べようと思ったが理解し易い文献が見当たらなかったため，
今後もう少し調べていきたい。
調査の中で，衝突による影響について考察している文献も発見した\cite{40015553613}が，それによると
1回のイオン化に\SI{30}{\electronvolt}程度消費されるとされており，
\SI{5.407}{\mega\electronvolt}の運動エネルギーをもった$\alpha$粒子が
\SI{4.0}{\centi\metre}走行する間には以下の式よりおよそ$1.8 \times 10^5$回
アルコールの原子と衝突していることが分かる。
\begin{align*}
  \frac{5.407\times10^6}{30} = 1.8 \times 10^5
\end{align*}

霧箱実験では，放射線の軌跡を非常に簡単に見ることができ，驚いた。
放射線は思いの外近いのだということを認識できた。

\printbibliography[title=参考文献]
\end{document}
